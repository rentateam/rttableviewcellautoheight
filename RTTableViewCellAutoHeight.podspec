Pod::Spec.new do |s|
  s.name             = "RTTableViewCellAutoHeight"
  s.version          = "1.0.0"
  s.summary          = "Height cacher for UITableViewCells."
  s.homepage         = "https://bitbucket.org/rentateam/rttableviewcellautoheight"
  s.license          = 'MIT'
  s.author           = { "a-25" => "a-25@rentateam.ru" }
  s.source           = { :git => "https://bitbucket.org/rentateam/rttableviewcellautoheight.git", :tag => s.version }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/*.{h,m}'

  s.frameworks = 'UIKit'
end
