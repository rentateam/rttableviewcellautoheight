//
//  RTTableViewCellHeightCacher.m
//
//  Created by A-25 on 24/09/15.
//  Copyright © 2015 rentateam. All rights reserved.
//

#import "RTTableViewCellHeightCacher.h"
#import <UIKit/UIKit.h>

@implementation RTTableViewCellHeightCacher

- (id)init
{
    if (self = [super init]){
        cellHeights = [[NSMutableDictionary alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMemoryWarning:) name: UIApplicationDidReceiveMemoryWarningNotification object:nil];
    }
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
}

-(CGFloat)getHeightForIndexPath:(NSIndexPath*)indexPath tableView:(UITableView*)tableView
{
    NSNumber *height = [cellHeights objectForKey:indexPath];
    if(height == nil){
        UITableViewCell *cell = [tableView.dataSource tableView:tableView cellForRowAtIndexPath:indexPath];
        CGFloat cellHeight = [self.resizer getCellHeight:cell tableView:tableView];
        [cellHeights setObject:@(cellHeight) forKey:indexPath];
        return cellHeight;
    } else {
        return [height intValue];
    }
}

-(void)deleteHeightForIndexPath:(NSIndexPath*)indexPath
{
    [cellHeights removeObjectForKey:indexPath];
}

-(void)deleteAll
{
    [cellHeights removeAllObjects];
}

-(void)onMemoryWarning:(NSNotification*)notification
{
    //Эх....
    [self deleteAll];
}

@end
