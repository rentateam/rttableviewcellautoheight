//
//  RTTableViewCellHeightCacher.h
//
//  Created by A-25 on 24/09/15.
//  Copyright © 2015 rentateam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTTableViewCellAutoHeightResizer.h"

@interface RTTableViewCellHeightCacher : NSObject
{
    NSMutableDictionary *cellHeights;
}

@property(nonatomic,strong) RTTableViewCellAutoHeightResizer *resizer;

-(CGFloat)getHeightForIndexPath:(NSIndexPath*)indexPath tableView:(UITableView*)tableView;
-(void)deleteHeightForIndexPath:(NSIndexPath*)indexPath;
-(void)deleteAll;

@end
