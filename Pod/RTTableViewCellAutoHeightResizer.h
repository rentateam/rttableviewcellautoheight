//
//  RTTableViewCellAutoHeightResizer.h
//
//  Created by A-25 on 24/09/15.
//  Copyright © 2015 rentateam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RTTableViewCellAutoHeightResizer : NSObject

-(CGFloat)getCellHeight:(UITableViewCell*)cell tableView:(UITableView*)tableView;

@end
